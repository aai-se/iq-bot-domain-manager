exports.GetAuthStatus = function(JsonResp){
	var jsonr = JSON.parse(JsonResp);
	var token = jsonr['token'];
	if(token){
		if(token != "timeout"){
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
};

exports.GetAuthToken = function(JsonResp){
	var jsonr = JSON.parse(JsonResp);
	var token = jsonr['token'];
	if(token){
		if(token != "timeout"){
			return token;
		}else{
			return "";
		}
	}else{
		return "";
	}
};
