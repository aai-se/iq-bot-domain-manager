var http = require('http');
var request = require('request');
var https = require('https');
var fs = require('fs');

exports.SendGenericPostHttpRequestWithToken = function(socket,host,port,authToken,myPath,data,myCall){
	var FULLURL = host+":"+port+myPath
	var FULLBODY = JSON.stringify(data)
	//console.log("DEBUG URL: "+FULLURL)
	//console.log("DEBUG BODY"+data)
	var options = {
		'url': FULLURL,
		'method': 'POST',
		'rejectUnauthorized': false,
		'body': FULLBODY,
		'timeout':6000,
		'headers': {
			'Content-Type': 'application/json',
			'X-Authorization': authToken
		}
	};

	 request(options, function (error, response) {
	   if (error){
			 socket.emit("server_side_error",error);
		 }else{
			 myCall(response.body)
			 //console.log(response.body);
		 }
	 });
};

exports.SendGenericPostHttpRequest = function(socket,host,port,myPath,data,myCall){
	var FULLURL = host+":"+port+myPath
	//var FULLBODY = JSON.stringify(data)
	//console.log("DEBUG URL: "+FULLURL)
	//console.log("DEBUG BODY"+FULLBODY)
	var options = {
		'url': FULLURL,
		'method': 'POST',
		'rejectUnauthorized': false,
		'body': data,
		'timeout':6000,
		'headers': {
			'Content-Type': 'application/json'
		}
	};

	 request(options, function (error, response) {
	   if (error){
			 socket.emit("server_side_error",error);
		 }else{
			 myCall(response.body)
			 //console.log(response.body);
		 }
	 });
};
/*
exports.SendGenericGetHttpRequestWithToken = function(socket,host,port,myPath,token,myCall){
	// setting standard options for https request
	var options = {
		host: host,
		rejectUnauthorized: false,
		port: port,
		method: 'GET',
		path: myPath,
		timeout:10000,
		headers: {
			'Accept': 'application/json',
			//'Accept-Encoding': 'gzip, deflate',
			'Accept-Language': 'en-US,en;q=0.9,fr;q=0.8',
			'x-authorization': token,
			'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
		}
	};

	var req = http.request(options, function (res) {
	  var STR_RES = "";
	  var chunks = [];

	  res.on("data", function (chunk) {
	    chunks.push(chunk);
	    STR_RES = STR_RES+chunk;
	  });

	  res.on('error', function(err){
		myCall(err);
		socket.emit("server_side_error",err);
		});


	  res.on("end", function () {
	    var body = Buffer.concat(chunks);
	    //VERY dirty fix....
	    if(STR_RES.indexOf("</html>") != -1){
	    	myCall(STR_RES.split("</html>")[1])
	    }else{
	    	myCall(STR_RES);
	    }


	  });
	});

	req.end();

};
*/
