var http = require('http');
var https = require('https');
var fs = require('fs');
var genws = require('./GenericWebServices');
var iq6utils = require('./IQBot6Utils');

exports.AuthRequest = function(host,port,data,socket){
	path = '/v1/authentication';
	genws.SendGenericPostHttpRequest(socket,host,port,path,myBody,function(res){
		//console.log("DEBUG Response:"+res);
		var jsonr = JSON.parse(res);
		var Status = iq6utils.GetAuthStatus(res);
		if(Status){
			socket.emit('auth_response','{"success":true,"message":"OK"}');
		}else{
			socket.emit('auth_response','{"success":false,"message":"'+jsonr['message']+'"}');
		}
	});
};

exports.ImportDomainRequest = function(host,authport,authBody,domaindata,socket){
	authpath = '/v1/authentication';
	domainimportpath = '/IQBot/gateway/domains/import';
	genws.SendGenericPostHttpRequest(socket, host,authport,authpath,authBody,function(AuthRes){
		var token = iq6utils.GetAuthToken(AuthRes);
		if(token == ""){
			socket.emit('server_side_error',"Could not get Authentication Token.");
		}else{
			genws.SendGenericPostHttpRequestWithToken(socket,host,authport,token,domainimportpath,domaindata,function(DomainImportRes){
			socket.emit('domain_create_response',DomainImportRes);
		});
		}
	});
};

/*
exports.AuthTokenRequest = function(host,port,data,socket){
	path = '/v1/authentication';
	genws.SendGenericPostHttpRequest(socket,host,port,path,data,function(res){
		//console.log("DEBUG RETURNED: " + res );
		var token = iq6utils.GetAuthToken(res);

		if(token != ""){
			socket.emit('auth_response','{"success":true}');
			return token;
		}else{
			socket.emit('auth_response','{"success":false}');
			return "";
		}
	});
};
*/

/*
exports.GetLearningInstanceList = function(host,authport,authdata,socket){

	authpath = '/v1/authentication';
	url_to_add = '/IQBot/api/projects';
	genws.SendGenericPostHttpRequest(socket, host,authport,authpath,authdata,function(AuthRes){

		var token = iq6utils.GetAuthToken(AuthRes);
		//console.log(token)
		if(token == ""){
			socket.emit('server_side_error',"Could not get Authentication Token.");
		}else{
			genws.SendGenericGetHttpRequestWithToken(socket,host,authport,url_to_add,token,function(response){
			//console.log(response)
			socket.emit('li_list',response);
		});
		}
	});
};
*/
