var socketio = require('socket.io');
var genutils = require('./GenericUtils');
var io;
const fs = require('fs');
var genws = require('./serverlib/GenericWebServices');
var iq5req = require('./serverlib/IQBot5Requests');
var iq6req = require('./serverlib/IQBot6Requests');

var DebugMode = false;

var JSONCONFIGFILE = './lib/config.json'
var JSONCONFIGFILE_TEMPLATE = './lib/template_config.json'
var JSONAPPCONFIGFILE = './lib/app_config.json'
var LANGUAGEFILE = './lib/languages.json'

// Start
var SERVEROK = true;
var SERVERTIMEOUT = false;

function processRequest(socket) {

// Deprecating the  "get_li_list" since A2019.x: IQ Bot now shows the OCR
/*
	socket.on('get_li_list', function(){
		getConfigData(function(MyConfigData){
			login = MyConfigData['login'];
			pwd = MyConfigData['password'];
			url = MyConfigData['url'];
			//version = MyConfigData['major_version'].trim();
			UrlInfo = genutils.GetRootFromURL(url)
			myBody = '{"username":"'+login+'","password":"'+pwd+'"}'
			//console.log("debug:"+myBody);
			if(version == "6"){
				iq6req.GetLearningInstanceList(UrlInfo['url'],UrlInfo['port'],myBody,socket)
			}
		});
	});
*/

	// Get List of supportedlanguages from config file
	socket.on('get_languages', function(){

			fs.readFile(LANGUAGEFILE,(err,data) => {
				if (err) {
					//console.log("error reading file config.json"+err);
					socket.emit('server_side_error',err);
				}
				else{
					socket.emit('languages_sent',JSON.parse(data));
				}
			})
	});

	// when getting a structure request from UI, send the JSONified content of the config file
	socket.on('get_structure', function(){
			// read the local config file
			fs.readFile(JSONCONFIGFILE,(err,data) => {
				if (err) {
					// if we cant find it then let's take a copy of the template one
					fs.copyFile(JSONCONFIGFILE_TEMPLATE, JSONCONFIGFILE, (err) => {
  						if (err) throw err;
  						console.log('INFO: Config File Not Found - Copied it from Template.');
  						socket.emit('structure_error',{});
					});
				}
				else{
					// Catching an error where the config file exists but is empty
					if(data.length == 0){
						fs.copyFile(JSONCONFIGFILE_TEMPLATE, JSONCONFIGFILE, (err) => {
  							if (err) throw err;
  							console.log('INFO: Config File Found but it is empty - Copied it from Template.');
  							socket.emit('structure_error',{});
							});
					}else{
						socket.emit('structure_sent',JSON.parse(data));
					}
				}
			});
	});

	// Removing the following as it is no longer used
	/*
	socket.on('get_mode', function(){
			fs.readFile(JSONAPPCONFIGFILE,(err,configdata) => {
				if (err) {
					socket.emit('demo_mode',false);
				}
				else{
					myConfigData = JSON.parse(configdata);
					DemoMode = myConfigData['demo_mode'];
					socket.emit('demo_mode',DemoMode);
				}
			});
	});
*/

	// when getting a structure save request from the UI, save the JSON passed as the new config file
	socket.on('save_structure', function(data){
		//console.log(data)
		var json = JSON.stringify(data);
		fs.writeFile(JSONCONFIGFILE, json, 'utf8', (err) => {
			if (err)
				socket.emit('server_side_error',err);
		});
	});

	// Request to import a new domain
	socket.on('import_json_domain', function(domaindata){

		// Get the CR credentials
		getConfigData(function(myConfigData){
			login = myConfigData['login'];
			pwd = myConfigData['password'];
			url = myConfigData['url'];

			myBody = '{"username":"'+login+'","password":"'+pwd+'"}'
			UrlInfo = genutils.GetRootFromURL(url)
			iq6req.ImportDomainRequest(UrlInfo['url'],UrlInfo['port'],myBody,domaindata,socket);
		})
	});

	// When testing the IQ Bot Credentials
	socket.on('save_and_test_iqbot_connection', function(res){
		// Save configuration locally (serer side)
		var json = JSON.stringify(res);
		fs.writeFileSync(JSONCONFIGFILE, json, 'utf8', (err) => {
			if (err)
				socket.emit('server_side_error',err);
		});

		getConfigData(function(MyConfigData){
			login = MyConfigData['login'];
			pwd = MyConfigData['password'];
			url = MyConfigData['url'];

			UrlInfo = genutils.GetRootFromURL(url)

			myBody = '{"username":"'+login+'","password":"'+pwd+'"}'
			myBodyDebug = '{"username":"'+login+'","password":"*****"}'
			console.log("debug:"+myBodyDebug);

			iq6req.AuthRequest(UrlInfo['url'],UrlInfo['port'],myBody,socket);
			console.log("Auth Request Sent To:"+UrlInfo['url']+":"+UrlInfo['port'])
			//iq6req.GetLearningInstanceList(UrlInfo['url'],UrlInfo['port'],myBody,socket)
		});
	});
}

function getConfigData(configHandler) {
  	fs.readFile(JSONCONFIGFILE,function(err,data){
  		if(data.length == 0){
  			console.log("Error reading config Data.")
  		}else{
	  		JSONConfigData = JSON.parse(data);
			CorrectedURL = JSONConfigData['url'];
			if(!CorrectedURL.startsWith("http") && !CorrectedURL.startsWith("https")){
				CorrectedURL = "http://"+JSONConfigData['url']
			}
			if(CorrectedURL.endsWith("/")){
				CorrectedURL = CorrectedURL.substring(0, CorrectedURL.length - 1);
			}
			JSONConfigData['url'] = CorrectedURL
			configHandler(JSONConfigData);
  		}

	});
}

exports.listen = function(server) {
	io = socketio.listen(server);
	//io.set('log level', 1);
	io.sockets.on('connection', function (socket) {
		processRequest(socket);
	});
};
