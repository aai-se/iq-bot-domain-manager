

function setSocketListeners(){

	socket.on('li_list',function(result){
		//console.log(result)
		addLearningInstancesToTable(result);
	});

};


function addLearningInstancesToTable(rawResponse){
	
	var jsonObj = JSON.parse(rawResponse.trim());

	LIData = jsonObj.data;

	for(var i = 0; i < LIData.length; i++){
		ITEM = jsonObj.data[i]
		LIID = ITEM.id;
		LINAME = ITEM.name;
		LIOCR = ITEM.ocrEngineDetails[0].engineType;
		
		
		$('#litable tbody').append('<tr>'+'<td>'+LINAME+'</td>'+'<td>'+LIOCR+'</td>'+'<td>'+LIID+'</td>'+'</tr>');

	}


		
}

$(document).ready(function(){

	setSocketListeners();

	$('#GetLIs').on('click', function() {
	    socket.emit('get_li_list');
	});


});

